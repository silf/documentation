
Requirements
============

.. warning::

    Not all features written in this document will be implemented, and this document doesn't
    contain any hard commitments.

.. note::

    Missing requirements:

    * Everyone may submit their own materials;
    * Enable users to up-vote materials;

Basic requirements
------------------

* We have a platform where partners (and third-parties) can upload content items.
* These content items may be structured in a linear fashion.
* Content types for these content items are TBD.
* Teachers can download and remix materials
* Students can participate in courses
* Materials are cloneable/remixable by third-parties


OLCMS requirements
------------------

**System is stable**
    System works reliably, and the urls are stable.

    This addresses :ref:`Adam's<story_adam>` story.

**Content is downloadable**
    Content types (for which it makes sense) are downloadable.

    Teacher should be able to download materials, for most material types:

    * Documents/textual content;
    * Quizzes, tests;
    * Videos;

    This addresses :ref:`Adam's<story_adam>` story.

**Teacher is in control of content scheduling**
    When teacher is presenting the contents in class, he is in control over scheduling
    --- he might need to skip some parts and/or focus on others.

    He absolutely needs to be able to **skip
    over content** --- LMS that strifctly enforces pre-requisites
    is totally unacceptable.

    This addresses :ref:`Adam's<story_adam>` story.

**LMS part**
    System should contain typical LMS part that allows students to
    review material on their own. Teacher would point student to LMS and
    then student could take course on their own

    This addresses :ref:`Cecilia's<cecilia_story>` story.

**System contains homeworks that are gradable, teacher can assess student performance**
    This addresses :ref:`Elize's<elize_story>` story.

**Content is browseable and searchable without login**
    This addresses :ref:`Daryl's<story_daryl>`, and :ref:`Alan's<alans_story>`.

**User is able to re-use content in way he is used to**
    Content is split in a small parts and user can download it and use it in
    whatever way he is comfortable with.

    Videos can be downloaded and embedded in presentations, pdfs can be printed,

    This addresses :ref:`Daryl's<story_daryl>` story
    and :ref:`Vangelis<_vangelis_story>`.

**OLCMS is translatable to many languages**

    We can translate OLCMS to different languages.

    We will try to translate it to as many languages as feasible.

    This addresses :ref:`Fabio's<fabio_story>`
    and :ref:`Vangelis<_vangelis_story>` story.

**OLCMS auto detects user's language and tries to display content's in this language**
    This addresses :ref:`Fabio's<fabio_story>`
    and :ref:`Vangelis<_vangelis_story>` story.

**User is able to switch this language**
    This addresses :ref:`Fabio's<fabio_story>`
    and :ref:`Vangelis<_vangelis_story>` story.

**OLCMS allows user to browse resources in other languages than his native.**
    This addresses :ref:`Fabio's<fabio_story>` story.

**OLCMS role is obvious even for users that found it via the search engine**
    User who arrives at OLCMS is able to quickly understand that:

    * He browses a repository of teaching materials;
    * He can search for more materials;

    This addresses :ref:`Grace's<grace_story>` story.

**User is informed on a Resource page what he might do with the resource**
    For example, when browsing Video resource, he should be informed that he might
    either show the video (and he gets link to Youtube) or he might download the video
    for off-line viewing.

    This addresses :ref:`Grace's<grace_story>` story.

**OLCMS is SEO friendly**
    This addresses :ref:`HawkBot's<hawkbot_story>` story.

**Users should be able to structure materials**
    User should be able to structure materials in a linear fashion, to create "lessons" from
    individual content pieces.

    This addresses :ref:`Adam's<story_adam>` story.

**Users should be able to edit materials on platform**
    If an instructor needs to change given content piece he might do so in a "forking" manner:
    that is --- he creates a copy of the resource which me might edit.

    This addresses :ref:`Adam's<story_adam>` story.

**Materials should contain metadata**
    To a possible extend metadata should be extracted from the uploaded contents. But content
    editor should be able to add his own metadata.


Search
------

* Full text search in material metadata as well as in material contents where possible.
* Tags
* Categorisation with respect to our ontology
* Discipline
* Language
* Pupil age?
* License?
* Content format


Content formats
---------------

**Content should be uploaded in editable format**
    Content's should be uploaded in editable formats,
    programs that can be used to edit uploaded materials ideally
    should be free and open source.

    Users should be able to "just" use content's in a way they
    are used to.

    This fulfills :ref:`Adam's<story_adam>` and :ref:`Vangelis<_vangelis_story>`.

**User should be able to view materials in free (and, ideally, open-source software)**

    Contents should be playable on variety of OSes and devices.

    This fulfills :ref:`Brina's<brina_story>`.

**Materials should be playable without installing new software**
    This fulfills :ref:`Brina's<brina_story>` and
    and :ref:`Vangelis<_vangelis_story>`.

    We should strive to use formats that have pre-installed players :

    * We can assume that recent version of Chrome/Firefox browser
      is installed everywhere.
    * Probably the same for Adobe Reader (for MS Windows systems)
    * We can assume users will be able to play videos.

**Printable documents should be uploaded in `.pdf`**
    But editable version (if one exists) should be uploaded alongside.

    This fulfills :ref:`Brina's<brina_story>`, :ref:`Adam's<story_adam>`.

**Formats designed for web should adapt to screen size**
    This fulfills :ref:`Brina's<brina_story>`

**Installation instructions are available**

    For each content format installation instructions are available
    for every platform that supports that format.

    This addresses :ref:`Astero<_story_astero>` story.

**Pedagogical instructions are available**

    For each resource we also give pedagogical instructions.


Course Guidelines
-----------------

**Use of cheap and available materials**
    If Instructor needs to buy some expendable materials for classes he should need
    to use cheap and available materials.

    Instructions should contain shops (including online shops) where he
    might buy the materials.

    This addresses :ref:`Brian's<story_brian>` story.


**Uploaded content should be splitted in small chunks**
    Some teachers will have very limited time for innovation,
    we should attempt to produce contents that is splitted in
    small parts:

    * Maximal duration for playing the content should be 15minutes.
    * All reusable parts should be separated.

    This addresses :ref:`Vangelis<_vangelis_story>` story.


**Materials should be printable**
    Content types (for which it makes sense) are printable.

    This addresses :ref:`Adam's<story_adam>` and :ref:`Vangelis<_vangelis_story>` story.

**Uploaded materials should have pedagogical guidelines**

    This addresses :ref:`Astero<_story_astero>` story.


Misc requirements
-----------------

Link to our github/gitlab/bitbucket profile is on the footer of every page.


Open problems
-------------

**Printability vs. adaptability**
    Ideally materials generated by partners should both look
    good when printed, and be adaptable to various screen size.

    There is no single format that fulfills both these requirements
    (``.pdf`` is good for printing, and ``.html`` or ``.epub`` are good
    at adapting to screen sized).

    Converting between these formats is not an easy thing to do.

Technical requirements
----------------------

**Use storage as a service solution**
    I'd rather not have to worry about backing up the resources too much.