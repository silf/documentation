School IT infrastructure
========================

Does assumption that every classroom has at least a single computer is valid in every classroom;
    Differs from country to country. In some countries it can be assumed in some other do not.

    Mostly we should go with: "If needed teacher will be able to get one laptop for classroom".

Does this computer have broadband Internet.

    Connection should be present.

    However materials should be downloadable nevertheless.

Does your schools have classrooms with computers on every student;

    There are IT classrooms.

Does assumption that every classroom has a computer for every student;

    Not really.

What OS-es school uses. Is this MS Windows or Linux.

    Mostly Windows, but there are a lot of Linux in Greece.

Does household of every student contains a PC-Computer

    Not really. Often it is the case, but not always.

If they do not own a computer, do they own another computing device: Tablet, Smartphone?

    Often they do. We should make everything work on smart-phone.

Does school in your country use any LMS software to deliver contents or tests?

    If they use something, they use Moodle.

