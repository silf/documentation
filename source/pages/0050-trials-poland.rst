Trial plan in Poland (WUT)
==========================

Status of this document
-----------------------

This is **not** a normative document --- I just asked our pedagogy team on
how they want to perform the trials, to share with this with the partners.

How will trials in Poland look like
-----------------------------------

We want teachers to use our materials in their schools.

For Polish teachers/schools using setting up the trails remotely is not feasible,
we need to travel to school (presumably multiple times).

On first visit we prepare a demonstration lesson --- that is a lesson that is
conducted by our staff entirely. Then on second visit we may ask teacher to
perform such a lesson themselves and get the evaluation forms filled. Teacher
needs to "see" our tools in action before he starts using them themselves.

Then we invite the school to visit WUT.

Problem points
--------------

1. Infrastructure is sketchy. We might have very different levels of technical
   capabilities for staff in our schools. Usually students are more capable than
   teachers though.

   One needs to book some time to install everything, and be prepared for surprises,
   we tried asking school staff to prepare computers (install some software) but
   it was rarely done.
2. Visiting the school is totally required. without it most probably they won't
   use the software.
3. Teacher training is important, but it doing it remotely is impractical in Poland.




