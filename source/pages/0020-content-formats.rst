Content formats
===============

For now we have more questions than answers.


General notes
-------------

Documents need to (also) in editable format. If you upload ``.pdf`` please upload whatever
format was used to generate the ``.pdf``.

Where should we put metadata? Either metadata should be a part of OLCMS data model, or
part of uploaded resources.

This is a `list of criteria for a content to be "Travelling Well" <http://eqnet.eun.org/web/guest/travel-well-learning-resources>`__
created as a part of another EC project.

Advice how to prepare each format
---------------------------------

Advices how to prepare content (formats and graphical presentation)

Printable documents
-------------------

TBD

* Experiments recipe (lots of metadata)


Requirements
************

* Format must be editable, or an editable version **must** be uploaded along with ``.pdf``.

Video
-----

TBD

Requirements
************

We definetely should have a single repository for video files, either created in-house or use
some generic solution like YouTube or Vimeo. Following features are considered to be essential:

* Support for subtitles
* Support for multiple subtitle tracks
* Support for multiple audio tracks
* Ad-free experience for our students (which excludes free versions of Youtube and Vimeo)
* Videos should be downloadable --- as Internet is not avilable in every classroom by default
  (This fuliflls :ref:`Adam's<story_adam>`),
* Video watermarks (TBD)

Open questions
**************

* Quiz formats
* What codecs will be a problem on WIndoes and what will not.

Requirements for produced videos
********************************

* Videos should be split to 5-10min parts
* Videos should be interleaved with quizzes that check understanding
  and keep student attention.


Interactive content (games, interactive experiments)
----------------------------------------------------

TBD

* Pdfs (interactive?)

We

Remote laboratories
-------------------

TBD


Questionaires // Quizzes
------------------------

I would envision these quizzes would be both downloadable and answerable online.

Things TBD
----------

* How to tackle content that is intended to be consumed both on-line and in print (actually most
  of the contents...)? On line content should adapt to screen size, which ``.pdf`` doesn't do, on
  the other hand ``.pdf`` is one of the format that reliably prints itself.

