Functionalities of the repository
=================================

All of this is still TBD!

Main entity in the system will be a "resource", which represents a single content item, that comprises
of many files. For example resource might contain:

* Youtube link to a video;
* Downloadable video version;
* Subtitles for the downloadable video in Polish;
* Subtitles for the downloadable video in English;


Requirements
------------

* We might store resources that contain multiple files;
* One of these files will be "default" (in this case youtube link).
* These files will be rendered (youtube links will embed to iframe, pdfs will be
  rendered using browser)
* Metadata is extracted from files, but users can add their own (predefined) metadata.
* Files in resources can be overriden and versioned.
* You can filter files in resource using language tags.
* (Stretch goal) There should be an easy way for an user to "fork" materials --- user will
  get a copy of a resource which he might edit to his needs.
* Multilanguage versions.
* Is integrated with S3 service or analogous. S3 service is a blob store as a service --- basically
  you upload static files there, and they are served from there. Backups, consistency, availability
  and handling transfer spikes is on their side. .