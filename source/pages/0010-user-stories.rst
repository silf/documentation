Use cases
=========

Educators
---------

.. _story_adam:

Adam --- innovative teacher from Poland
***************************************

Adam is a Physics teacher who teaches at a local high school,
he wants his lessons to be as attractive as possible, and also wants
to encourage as many students to STEM fields as possible.

He uses materials provided by the STEM4youth project, to enhance
his lessons:

* He displays movies as a part of his presentations.
* He uses tests developed in the project, as a part of
  homework assignments.
* He performs remote laboratory experiments to show
  physical phenomena to students.

What is important for Adam
^^^^^^^^^^^^^^^^^^^^^^^^^^

1. He needs stability --- if he needs to perform the experiment
   during the lesson --- this experiment needs to be online.
   There is not so many hours in the Physics curriculum in Poland, so every
   interruption will be painful.
2. He needs to be able to download or embed various materials.
   He should be able to present those materials during the lessons, even if
   there is no Internet connection in the classroom.
3. Adam would like to be able to structure resources in linear
   fashion, e.g. after displaying video about photoelectric effect,
   he'd like to show students some example test questions, and after
   perform experiment on-line. This structuring should be doable
   also outside of LMS context.
4. During the lessons he needs to be in control what
   is displayed right now, students might have problems with some
   concepts --- which he might need to explain in depth ---
   rescheduling some contents.

   He really doesn't need an LMS to control order in which
   content is given. He absolutely needs to be able to **skip
   over content** --- LMS that strictly enforces pre-requisites
   is totally unacceptable.
5. Sometime Adam needs to make a small adjustment to the materials ---
   they should be editable.
6. Materials for students need to be printable, in Adam's school
   there is only a single classroom with computers, which is
   spoken for most of the times.

Problems with this user story
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None known

.. _story_astero:

Astero --- STEM teacher in Greece
*********************************

Astero has a four year degree in Physics. She has a good knowledge of Mathematics and some
experience with computing. She doesn't have much time to prepare for an innovative
lesson neither sufficient freedom to deviate from the curriculum.
Her lessons usually have a linear structure, but she also has to use her improvisation skills since things do not always work as planned.

Astero is using the STEM4youth project material to make her lesson more fancy:

* She wants to have online and offline access to the repository material.
* She wants to be able to select from the available material (video, simulations, worksheets etc.)
  in the platform in order to integrate it to her ordinary lesson.
* Her lessons usually have a linear structure, but she also has to use her improvisation skills since
  things do not always work as planned.


What is important for Astero
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* She teaches in many schools with different equipment, she needs to know:

  1. Which materials are compatible with the installed OS (Windows XP/7/8/10, Linux)
  2. How to use the material in the given OS (if some setup is necessary)

* She'd like to have pedagogical guidelines along with the content.

.. _story_brian:

Brian --- local activist and STEM enthusiast
********************************************

Brian is leading a local institution devoted to education
of children (let's say Kids club in a local Culture Centre).
His lessons are not obligatory --- so they need to be
super attractive.

He is focused on developing hands-on activities, and he'd like
to use one of the activities prepared in the STEM4youth
project. His work in the Culture Centre is either
voluntary, or not his main activity --- he won't be able to
spend too much time preparing.

What is important for Brian
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* He can easily order everything needed for the hands-on
  activities, if something is not available in typical
  supermarket/general store he either gets a list of outlets
  in Europe, or a internet shop address.
* Parts are cheap.
* He has guidelines for whole hands on activity.


.. _cecilia_story:

Cecilia --- teacher
*******************

Cecilia is a maths teacher in an Arts high-school, she
has less hours for Maths than typical schools, moreover here
pupils are less interested than average in the matter.

Typically only interest in Maths is in the final
year of high school, where students start to be concerned
with the final exams, where math is obligatory. However
she still has not enough hours to properly review whole
curriculum. She'd like to have a tool which allows students
to review several parts of the curriculum, independently
on their own pace --- she'd like to direct them to a webpage,
and give some pointers on what to learn.

What is important for Cecilia
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* She doesn't want to interact with the platform,
  she is extremely busy.
* Whole experience for students should be canned ---
  no help from her should be neccessary.

Problems with this user story
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If we implement this story for a given course:

* Exercises would need to be prepared in a special format (like GIFT)
* Possibly they would be then converted to .pdf for printing (if we want it!).
* LMS needs login functionality for students.


.. _story_vangelis:


Vangelis --- a retiring teacher
*******************************

Vangelis is an old teacher --- he still cares deeply about
his students, so he tries to use whatever best materials
are necessary, however due to his digital exclusion has only limited knowledge of IT.
He doesn't know what is "Google +" and doesn't use
"Facebook".
He follows old fashioned teaching methods, but he likes to introduce some innovative elements in his
lesson. He teaches Mathematics. Vangelis doesn't speak English.

Vangelis is using content from the STEM4youth project in order to be in touch with new developments
in pedagogical methods and tools. He believes that by integrating content he will make his
lessons more attractive and interesting to his technologically informed students.

* Vangelis is more familiar with printed material.
* Vangelis wants the content to be easily integrated to his old fashioned teaching.
* Vangelis has limited time within each lesson available for innovative content.

What is important for Vangelis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Login should be unnecessary to download materials from
  repository.
* OLCMS should allow downloading the materials in
  a easy to use formats.
* Vangelis wants any content to be compatible with Windows because it is the only OS
  he is familiarized with.
* Vangelis wants the software content not to need any third party installation or preferably any
  installation at all
* Vangelis needs the content to be of a short duration.
* Vangelis wants to be able to browse easily the content at the OLCMS

.. _zuzanna_story:

Zuzanna --- innovative teacher from Poland (homework part)
**********************************************************

Zuzanna is a Physics teacher who teaches at a local high school.
Her students, although they are interested in subject, don't seem
to work hard in home on Physics. This is mostly due to
following factors:

* In Poland Physics is not obligatory on final exams (Maths is),
  and students are more concerned with obligatory subjects.
* Students have attention span problems --- they have only a single
  hour of Physics in the week so it's easy to forget.
* Some student's are lazy and just copy homework from others
  before the lesson
* Zuzanna has no time to check everybody's homework all the time.

Zuzanna is also a very busy person --- she teaches in three schools.
She would like to give more interesting homework, but she has
no time to prepare it --- so she usually asks students to do some
problems from the problem book.

What is important for Zuzanna
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* She'd like to have an external system in which he would
  schedule homework for his students.
* Homework should be graded automatically.
* Each student should see slightly different questions.

Problems with this user story
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The same as for :ref:`Cecilia<cecilia>`.

.. _fabio_story:


Fabio --- search
****************

Fabio is a teacher coming from outside of the project, he has no idea
what STEM4youth is and needs to find what materials are in the repository.

He is a biology teacher teaching in polish school, however in his shool
all classes took part in Italian.

He enters "Biology" in the search box, and we'd like him to:

* Find materials related to the Medicine course.
* Immediately see that materials are in Italian and English.

.. _grace:
.. _grace_story:

Grace --- teacher that stumbles upon OLCMS
******************************************

Grace was searching the Internet for materials she might use for her lesson.

We want her to:

* Know what OLCMS is.
* Be able to search OLCMS for contents.
* When she find matierials in OLCMS, she knows how to use them.

All above scenarios should be fulfilled when search engine redirects
Grace to:

* OLCMS landing page
* OLCMS search result page
* OLCMS page for a resource.


.. _hawkbot_story:


HawkBot --- search engine bot indexing OLCMS
********************************************

Hawkbot is a search index bot that indexes OLCMS.

We want HawkBot to be able to properly index OLCMS,
giving this page a high ranking.


Consortium members
------------------

Annemarie --- IT specialist
***************************

Annemarie is an IT specialist, she works with the pedagogy team
to prepare a set of interactive exercises that the student's use.

She needs to have specs for the content that will allow:

1. Seamless integration with OLCMS.
2. Allow students to be graded.

Bertram --- video specialist
****************************

Bertram is a video specialist responsible for recording and subtitling
the video.

Bertram needs to have:

* Defined video format;
* Defined subtitle format;


Cedric --- project manager
**************************

Cedric is a project manager, he is concerned that we signed keeping project
output on-line for two years after the project finishes. He wants almost all
products to be hosted on OLCMS --- this way if we keep OLCMS on-line we will
fulfill our promises.

Derrick --- course manager
**************************

Derrick is a person uploading course materials to OLCMS. He needs to upload multiple
versions of the matersals in two languages, he wants this process to be as organized
as possible.

He has following requirements:

* Resources should be editable --- user with proper permissions should be able to
  change resources at will.
* Each resource should have language versions. Derrick wants to have as few resources
  as possible --- it is easier to upload two changed language versions for a resource,
  than a upload two separate resources.
* For each part of the resource we should display change date, and possibly change history.

Students
--------


.. _alans_story:


Alan
****

He has low technical skills, if he has to access materials in the repository
he shouldn't have to log in.


.. _brina_story:


Brina
*****

Brina is a high school student that, due to financial constraints
doesn't own a computer. She however owns a smartphone that come "free"
with her carrier plan.

What is important for Brina
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Most of the materials should be available (in one way or the other)
  on low resolution smart-phone screen, using a typical smartphone OS
  (e.g. Android).

Problems with this user story
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All documents accessible on the smartphone:

* Should be in a format accessible on a smartphone (definetely not MS WORD)
* Should be in a format that adapts to screen size (not .pdf).
* Even if she has PC computer --- with MS Windows system, she might not
  have MS Word license --- so we should rely on formats that have freely
  available viewers.

Calvin --- student who founds this page using search
****************************************************

This scenario is similar to :ref:`Grace<grace>` --- user enters OLCMS from
search engine, but this time we want him to:

* Get best/highlighted content/most engaging content and then use it.

Unrelated third parties
-----------------------

Alexis
******

Alexis is a developer working on a related project, he is trying to discover landscape of
OER repositories and stumbles upon our site.

He should immediately know that this site is based on open-source software and know that
he is welcome to fork and extend it.
