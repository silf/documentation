Terms dictionary
----------------

OLCMS
    stands for Open Learning Content Management System.

Resource
    Single thing that is submitted to OLCMS, resources can contain multiple files.