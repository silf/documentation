Integration with youtube
========================

Videos will be one of important content types produced in this project, in the
requirements gathering phase we have found following requirements:

1. Videos need to be playable on a variety of devices, including portable ones.
2. Users will have Internet connection of different quality. User should be able to increase
   and decrease quality of the video as needed.
3. Videos need to be downloadable.

To accommodate these needs we have decided that most interaction of end-users with video
materials will be done through Youtube service (Youtube is a leading video streaming website),
videos uploaded to Youtube are viewable on a variety of devices, and can be streamed in a couple of
quality versions, so by uploading all videos to Youtube we can accommodate requirements 1 and 2.
Moreover youtube has applications for most major smart-phone and tablet brands.

Videos will also be stored inside the OLCMS repository to serve as a backup copy and to facilitate
easy downloads of the video for the end-user.

Uploading video will have following flow:

1. Upload the video to OLCMS Repository.
2. If avilable upload subtitles.
3. OLCMS will upload the video and the subtitles to youtube.

Videos will be uploaded by the OLCMS (and not by the end-user) as it is important that version
uploaded to youtube be the same as the one that is downloadable from the OLCMS repository.

To upload the videos we will use Youtube API. Youtube API has a python client library that is
supported and developed by the company that operates Youtube.

Youtube API uses quota system that allows up to 400 video uploads daily (these limits can be
extended), these limits should suffice for the OLCMS use by the consortium partners.
