Integration with forum
======================

We will use off-the shelf forum software, right now most likely candidate will be
`Discourse <https://www.discourse.org/>`__, which is a modern forum solution. Discourse was
selected for following reasons:

1. It is a modern software written in a modern software stack (Ruby On Rails)
2. It is an open-source software.
3. It has an extensive API, which allows total control over Discourse.
4. It supports Single Sign on with relatively simple API. This API is described here:
   https://meta.discourse.org/t/official-single-sign-on-for-discourse/13045 .

We will consider following integrations between OLCMS and the forum:

1. Allow single-sign-on, that is when user is logged to the repository he will be automatically
   logged into forum as well.
2. Use forum as a solution to show comments under various resources uploaded to OLCMS.

