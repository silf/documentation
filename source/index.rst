.. STEM4youth Documentation documentation master file, created by
   sphinx-quickstart on Fri Jun 17 13:18:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to STEM4youth Documentation's documentation!
====================================================

Contents
--------

.. note::

   You may `download source for this documentation
   <https://bitbucket.org/silf/documentation>`__ here.

   If a consortium partner is familiar with GIT/bitbucket and
   `restructuredtext <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`__
   they are more than welcome to submit Pull Requests to the documentation.

.. toctree::
   :maxdepth: 1
   :glob:

   /pages/*

Questionnaires
--------------

.. toctree::
   :maxdepth: 1
   :glob:

   /questionaries/*

