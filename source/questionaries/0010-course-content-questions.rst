Trial questions
===============

Content types
-------------

What kinds of contents you wish to provide (movies, exercises, games, lecture texts,
presentations etc.) to our repository. For each type of content (presentation, exercise etc),
please provide in what format will it be provided, for example:

1. For lectures will it be: OpenOffice documents, MS Word files or PDF?
2. For movies what format will movie be in: mpg, ogv, or will it be posted to youtube, etc.
3. Exercises will it be pen and paper exercise, or will it be a interactive application
   that performs grading?
4. Some of the contents will need to be translated by third parties eg. to perform trials,
   how will you facilitate this.
5. Will your content work on non-computer device (tabled, smart phone).

Please treat above questions as an example ones. Feel free to give me as much information
as necessary.

Content consumption by schools
------------------------------
How, do you envision, this content will be consumed by high-schools in your country?
Here are some guiding examples to what I'd like to know:

* Will teachers install some third party software (eg. moodle) to deliver the exercise,
  or will it be just plain old pen and paper?
* Will they use movies you provide as part of their presentations?
  Will they use your movie when presenting some phenomenon?
* Will your educational games be played in classroom, or in home --- will this game grade students?
* How your content will be consumed by students?
* Will individual pieces of content (movies, games, exercises) be assembled to larger pieces:
  whole lessons, fragments of curricula or whole curricula? Will these assembly be done by you,
  or do you intend to allow teachers to do this easily

Please treat above questions as an example ones. Feel free to give me as much information
as necessary.

How will the trial be performed?
--------------------------------

1. Do you wish to engage schools in your country?
2. Does trial require teacher training? How it will it be performed? Will the training
   be performed on-line, or will trainers need to meet with the teachers.
3. Will educators/representatives from your institution be present in the schools?
4. Does the trial involve shipping physical goods (experiment sets)?
5. Do you wish to perform trials in science fairs (science festivals)?
6. How will online/interactive content (you produce) be used in trials in schools?
7. How will online/interactive content be used in trials outside schools
   (e.g. in science fairs)?

Please treat above questions as an example ones. Feel free to give me as much information
as necessary.

Schools in your country
-----------------------

1. Does assumption that every classroom has at least a single computer
   is valid in every classroom; Does this computer have broadband Internet.
2. Does your schools have classrooms with computers on every student;
3. Does assumption that every classroom has a computer for every student;
4. What OS-es school uses. Is this MS Windows or Linux.
5. Does household of every student contains a PC-Computer
6. If they do not own a computer, do they own another computing device: Tablet, Smartphone?
7. Does school in your country use any LMS software to deliver contents or tests? If so
   what software they typically use. For example: in Poland they use Moodle.

