Stored experiments discovery
============================

If experiment hardware is off-line, malfunctioning or just being currently used
we want to allow user to "perform" or rather "watch" pre-recorded experiment.
with only slightly degraded experience.

Blockers
--------

This epic has following blockers:

1. Monitoring service --- we'd need to have that in place beforehand, if we want to present
   user option to perform the "stored" experiment when there is a malfunction we need
   to monitor service state.

Possible solutions
------------------

Use recorded data
*****************

We'll just "record" data from 10-20 runs of experiment sessions and enable users to "view"
these sessions.

During the "real" experiment runs we would store:

* Partial results (each instance of ``silf:results`` stanza)
* Timing information (time between each result)
* Current frame in camera (one for result frame)

Advantages
^^^^^^^^^^

1. Either no work, or minimal work (configuration only) per experiment.
2. No new components added to the system.
3. We show "real" data (whatever that means...)

Disadvantages
^^^^^^^^^^^^^

1. We will constrain parameters user will be able to choose --- that is: we will allow them to
   specify only input parameter values that were already pre-recorded

Open problems
^^^^^^^^^^^^^

1. Should we record each session, or rather: should we record sessions of "normal users"?
   We do allow to download results of session ONLY if user was taking part in it, if we'd
   allow any random user to replay the session and download the results we'd lose that
   assumption.

   Probable solution: add user-wide setting (and give it to Janek) so each of his session is
   stored.

   NOTE: Storing results for every experiment performed experiment has it's appeal though.
2. How this interacts with the bot? At the very least we should make sure that bot sessions
   are pre-recorded.
3. Should we also somehow take into account hour of day --- it will affect lightning conditions?
4. How to cope with live settings? There are not present on all the experiments, but when we
   add light switch --- they will be (or just drop the light switch idea, and turn-on light
   always).


Use simulated data
******************

Use simulated data or semi-simulated data. In this case we build some semi-heuristic-semi-analytic
model of the results.

In essence this would work similarly to previous solution, but:
for each input parameters model would be able to "generate" series of results, and match
camera footage to it.

Example
^^^^^^^

Maybe an example: we have a Black Body experiment, there are following settings there:

1. Start of the scan (wavelength)
2. End of the scan (wavelength)
3. Source temperature
4. Number of points

We gather scans on maximal width with good resolution for 10-15 temperatures, then
we have enough data to create interpolating model that allows one to estimate
value of voltage for each wavelength for each temperature.

Now when experiment starts we are able to generate data for each combinations for input parameters.

Open problems
^^^^^^^^^^^^^

1. How to provide footage in this method? Probably I could hand-wave some approach but let's
   ignore that!

Solution evaluation
*******************

I think we should go for first solution.


Work to do
----------

.. figure:: img/stored-experiments.png

   Architecture


Experiment saving
*****************

When to save experiment
^^^^^^^^^^^^^^^^^^^^^^^

I'd say: we should store all experiment runs, rationale is following:

1. There is not too much data, it won't be TB's of data. If we start feeling that there is too much
   data we might just: A) delete pictures B) Stop picture storing.
2. It might enable some interesting (publishable!) insights on how users play with this tool.

On the other hand only some experiment runs should be re-playable. I'd say that users with proper
rights should be able to mark series as re-playable using Django Admin Interface.

.. note::

    It **might** be possible that we'll need to mark each result as being 're-playable', but
    this is out of scope for this story.

    If such need arises we'll need add a new story.


On the other hand we should have a ready endpoint to suppress storing of the experiments. Right now
experiments will be stored always --- which might lead to performance decrease if we'll get hit
by time-intensive trials, to protect against this risk there should be a relatively easy way
to suppress whole functionality.

How does photo storing work
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Athena contains a client for the protocol of our camera server, it connects to this server at the
start of the experiment and starts downloading images.

When it receives ``silf:results`` stanza it selects image with nearest timestamp.
Then it sends this photo to ``silf-app`` for storage, ``silf-app`` responds with
opaque handle to that image, this handle is then inserted into ``silf:results`` stanza as additional
parameter, patched stanza is stored in the DB.

.. note::

    Most probably each stanza will be a separate row in the database, however this is an
    implementation detail, and we might transition to a less normalized shema.


For each ``silf:results`` we store exactly a single photo.

I want all our services to be stateless, so photos won't be stored on the service filesystem.
First storage backend we will evaluate will be Swift/S3.


**Open problems**

How to solve problem of timing differences between servers?
    Best solution would be to add timestamps to all messages and match using that. Problem of
    de-synchronized system time should be solved by using NTP daemon. This could lead to some errors
    in network time, however we will be synchronizing camera server with experiment server, and these
    services are very close together (in the same local network) so any NTP discrepancies should be
    the same for both of them.



Step by step flow of storing experiment data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Athena receives ``silf:session:start``, then it sends data package containing following data:
   ``username`` of the operator, ``session_id`` and name of the experiment. If ``silf-app`` decides
   that it should store this session it returns ``201`` status, if session can't be stored it should
   return ``406`` (in this cases athena doesn't do anything more).

   .. note::

        By default silf app will always store the experiment, however I want a quick way to
        disable this feature, and patching the method to return `406` status is good enough.

2. On each ``silf:results`` athena:

   a. Matches image to the results.
   b. Sends the image to ``silf-api`` receiving opaque handle.
   c. Inserts the handle to ``silf:results`` stanza under some property and saves that to ``silf-app``.

3. On ``silf:session:stop`` it sends notification to ``silf-app`` storing who stopped the session.


.. figure:: stored-experiments/athena-image-service.png

    Athena saving the images

.. figure:: stored-experiments/store-experiment.png

    Store experiment flow

.. figure:: stored-experiments/cant-store.png

    Flow when storage is disabled

Experiment playing
******************

.. _stored-spec-retrieve-img:

How does photo retrieving work
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Images will be stored either in the database or in the Swift/S3 storage, and I'd want to be
able to switch these storage due to whatever reason we deem necessary. This switch should not
require me to change every stored experiment.

Athena will always contact ``silf-app`` to get handle for the file.

So the photo retrieving works as follows:

1. Photos are identified by opaque handles.
2. When ``silf-app`` gets a query to download an image it can either: A) send image contents
   B) send ``302`` response --- that client should follow. Picture is stored under the redirected
   resource.

Step by step flow
^^^^^^^^^^^^^^^^^

1, When checking the experiment ``../api/experiment`` endpoint Mercury should get information
   whether the experiment is on, if there are some problems it should give information to the
   end-user somehow about the fact, and should proceed with re-playing the experiment.

   .. note::


        Most probably there should be a GUI to allow user to explicitly enable replay experiment
        even if monitoring service says everything is OK.

2. All input fields are replaced with combo boxes, Mercury queries ``silf-app`` for possible
   values in all combo boxes. When user selects each field Mercury queries for new values
   (see: :ref:`stored-spec-input-storage`). When there is only a single choice left all blank input
   fields are filled.
3. Since last response contained a list of series for this set of input parameters Mercury can
   start session. There might be more than one series for given set of input parameters --- then
   Mercury selects replayed series at random (this way we don't show the same data for each replayed
   session).
4. Mercury sends stanza in new namespace ``silf:storedsession:start``, this stanza contains input
   fields, and selected stored session.
5. Stanza: ``silf:storedsession:start`` is handled by Athena (we put this in different namespace
   only to not have it interpreted by the experiment by mistake!). When athena sees this experiment
   it sends ``silf:storedsession:start`` response to this message containing:

   a. List of urls for images (sorted by timestamp) to prefetch for this session.
   b. Session id.
6. Mercury tries to have about 10 images pre-fetched.

   .. note::

        When we have many observers this could put some load on our servers.

        This is why I want to have images in Swift/S3 service.

7. At appropriate intervals Athena sends ``silf:results`` containing new results (obviously Athena
   should not interpret them).
8. After the session finishes Athena sends ``silf:storedsession:stop`` (content the same as
   ``silf:stop`` but different namespace so experiment won't interpret it).

.. figure:: stored-experiments/stored-experiment-play.png

    Playing the stored experiment

Breakup into tasks (more discoveries)
-------------------------------------

Try to figure out how this issue can also fix historical sessions
*****************************************************************

See :ref:`spec-stored-historical-concern`, if possible modify this spec accordingly.

.. _spec-stored-protocol-extensions-task:

Write protocol extensions for this  feature
*******************************************

Following things need to be accommodated:

1. Adding photos from camera sa links to the results.
2. Id's for the experiment results (``silf:results`` stanza) this might be another UUID.
3. Adding timestamp to all messages. Timestamp is in UTC.
4. ``silf:storedsession:start`` messages

Document set of enhancements to the API provided by ``silf-app``
****************************************************************

Read this document and define specific APIs (endpoints, methods, data structures) that ``silf-api``
will follow.

Find out what is the current state of saving the experiment results
*******************************************************************

Find out what is the current state of experiment results in
``silf-app`` and ``athena``. Fix any issues and make it work.

1. Check if we store mode in the results, if not add it.
2. Add some tests.

Blocked by: Deployment in locally

Discuss with Janek how ui should look like
******************************************

1. Discuss with Janek how stored experiment UI should look like.

   .. note::

        be carefull, nothing fancy - should be as simple as possible

2. Create some mockups.

Breakup into tasks (concrete tasks)
-----------------------------------

Draft required changes to the protocol
**************************************

Write down required changes to the protocol.


Find out what protocol we use to fetch camera images
****************************************************

Camera server is **probably** here: `backend-bachus-websockets
<https://bitbucket.org/silf/backend-bachus-websockets>`__. reverse engineer that protocol, and
describe it, describe how to install and run Bacchus server, and what are external dependencies
to that.

Optionally:

1. Create Ansible deployment scripts so we may deploy that automatically (if you are not comfortable
   with it add this as a follow-up task).
2. Create a mock server that doesn't interact with the camera (lets say: sends frames containing
   images of numbers from 0 to 1000).
2. Describe possible improvements to it.
3. Update the protocol to handle sending of image timestamps.

Implement python client for the camera protocol
***********************************************

Implement a python client for the camera protocol, there are following requirements:

1. It should store (in memory) a list 10 most recently sent images, with the timestamp
   when they were received. Number of stored photos should be configurable.
2. It should spawn no processes.
3. Possibly it will need to spawn a new thread
4. Use some async api to fetch the pictures (why not?) might be fun :)


.. _stored-spec-input-storage:

Prepare API of silf APP to store experiment results
***************************************************

Experiment results store almost what we need, but:

1. Intermediate result are stored (need to store every stanza received from the experiment) --- right
   now only final results are stored.
2. Initial settings are not stored in an way that can be indexed --- and will be queried.

Fix these issues with data model, and also:

1. Store intermediate experiment results as well as time when they were received,
   --- relative to the experiment start. Stanzas should contain a timestamp so use this timestamp.
2. Add boolean switch to series: labeled ``can_be_replayed`` (defaults to false).

   Add UI to django admin to set this to true.

3. Add to silf-api ability to retrieve:

   a. Stored experiment session by settings.
   b. All results for session (paginated?)
4. Add ``stored_experiments_enabled`` which is true iff there are stored session associated with
   this experiment.

Add ability to query for input settings
***************************************

Add ability to query for input settings.

Let's assume that an experiment has following settings:
``freq_start``, ``freq_end``, ``resolution``, ``temperature``.
User has selected settings ``freq_start`` and ``resolution``. This api would answer following
question what are possible values for ``freq_end`` and ``temperature`` for which we have stored
data sets for:

* ``freq_start``, ``freq_end``, ``resolution`` (and whatever temperature)
* ``freq_start``, ``resolution``, ``temperature`` (and any ``freq_end``)

This API will be queried before user fills any settings to get all possible values
for the parameters. After the user fills each setting it will be queried again to find
what values are possible in combination with already filled settings.

Results should be relative to experiment and experiment mode.

Additionally responses will contain a list of series that satisfy current search conditions.

Add storage of photos to silf app
*********************************

Add API to store images to SILF-APP, photos should be stored along with,
results they are attached to.

Use standard django storage to store the files.

Allow any user to download these images.

Following information should be stored for the image:

1. Session id
2. FK to experiment

Images should be queried using opaque handle.

**Image retrieval**

See: :ref:`stored-spec-retrieve-img`

**Queries supported**

1. All images for a series (returns opaque handle)

Integrate Athena with new stuff in the API
******************************************

At the end of this task Athena should use APIs to store:

1. Intermediate experiment results
2. Intercept images from camera and store them in ``silf-app``

Implement replaying in the Athena
*********************************

Implement support for ``silf:storedsession:start`` and assorted things.


Update mercury to use ``silf-app`` API
**************************************

Note full spec for changes will be defined in the
:ref:`protocol extension task <spec-stored-protocol-extensions-task>`.

1. If experiment contains has ``stored_experiments_enabled`` present user a button to
   enable stored mode.
2. In this mode each input field is a combo-box from which you can preselect values.
3. Live fields are disabled.



.. note::

    I briefly wandered if we can't create a new websocket server that will serve as "fake" camera
    for the Mercury in stored mode, but I decided against:

    * Would introduce new set of race conditions --- when results de-synchronize with the images.
    * One more component to care for

Open problems
-------------

Scalability
***********

These do not address problem of scalability --- whole communication is still done in the same
room, and "normal" experiments.

For now this is OK, I guess.

.. note::

    Do decide how to handle this use case we should decide whether in "scalability mode" experiment
    should behave in the same way as in "normal mode", i.e. provide ability for many people to
    watch the same results, and chat with each other.

    If this is required changes will be very big, if we can drop this requirement we can just
    implement direct (direct as in not using groupchat) XMPP messages between Mercury and Athena.


.. _spec-stored-historical-concern:

Some similarities to ``:historical`` namespace
**********************************************

This has some similarities with ``:historical:`` stanzas that were designed (but not implemented)
to serve results, for user that joined the experiment when this experiment already was in progress.

It would be good to leverage these similarities to have only single feature of these two
implemented.

Future enhancements
-------------------

1. Use this feature to enable better scalability
2. Allow ChatBot to perform stored sessions.
