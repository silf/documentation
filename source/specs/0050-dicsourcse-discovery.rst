Discourse installation
======================

Changes to ours servers:

1. Add swap to our system (find ansible role)
2. Bump disk size

Installation goals
------------------

It would be great if we could just deploy discourse as one more image in our code, at the last resort we could
just use dedicated VM but costs.

Install discourse
-----------------

There are three ways to install discourse on our servers, all of them are fucking weird.

1. Use official version that uses docker, but not **docker-compose**, and some really weird ``yaml`` templates that
   are not ansible but some really weird ruby stuff.

   Discourse devs are rather sure that they want these weird things:

   * https://meta.discourse.org/t/can-discourse-ship-frequent-docker-images-that-do-not-need-to-be-bootstrapped/33205/67

2. Use some unsupported ``docker-compose`` repositories in github, `like this one <https://github.com/indiehosters/discourse>`__
   these are cool, **but** I'm not entirely sure they will be as fast and as secure as official one.

   For starters `nginx conf in official image
   <https://github.com/discourse/discourse/blob/master/config/nginx.sample.conf>`__ has some security checks missing
   from one in `in docker-compose image <https://github.com/indiehosters/discourse/blob/master/nginx.conf>`__.

3. There is an `ansible script to deploy discourse
   <https://github.com/SamSaffron/discourse-ansible/blob/master/roles/discourse/tasks/main.yml>`__

   We **dont** want to use ansible, but maybe we could re-write ansible scripts as docker files for Nginx and Discourse
   roles.

Why official docker is the only way
===================================

`Accodrding to this discussion
<https://meta.discourse.org/t/ansible-discourse-install-discourse-with-ansible-without-docker/31215/9`__ docker image
use some crazy shit ruby patches, and speedups, quoting:

.. pull-quote::

    The Ruby 2.2 image we have (which is enabled on meta) is a custom build of Ruby with method cache patches.
    Unfortunately, even patched we are struggling to meet the same memory and cpu perf we get on 2.0,
    hence it is not yet default.

Which basically means that we are stuck to this fucking docker setup I don't understand a shit.

Decision
--------

We should try to generate normal images containing discourse code. By:

1. ``FROM discourse:version``.
2. Either manually applying the "yaml templates" during build phase, or just apply them using this pups thing but duting
   build.
3. Or just use this bootstrap thing and then `https://docs.docker.com/engine/reference/commandline/commit/`.

Possible alternative: use another forum backend, like this: https://flaskbb.org/

Customising discourse
=====================


General things
--------------

How to update settings without using UI (probably database).

Rest API Client:

* https://meta.discourse.org/t/discourse-api-documentation/22706
* https://meta.discourse.org/t/using-the-rest-api-with-other-languages/21699

SSO
---

Single sign on: https://meta.discourse.org/t/official-single-sign-on-for-discourse/13045

.. todo::

    Find out how to configure SSO automatically, without having to, well click-through things.


How to automatically create topics
----------------------------------

Via REST API, or create them automatically by embedding javascript.

How to embed threads inside our content items
---------------------------------------------

* https://meta.discourse.org/t/using-the-rest-api-with-other-languages/21699
* https://meta.discourse.org/t/embedding-discourse-comments-via-javascript/31963

Things that are nice but not required
-------------------------------------

* https://meta.discourse.org/t/set-up-reply-via-email-support/14003


List of tasks
-------------

List of jira tasks in this epic:

1. Increase disk sizes on ours systems
2. Update ansible scripts to have swap file
3. Dockerize discourse (partially done)
3. Find a way to update discourse settings (SSO, admin users, etc.) automatically: without clicking to gui.
4. Implement SSO
5. Add snippet that automatically creates discussions for a given content item
