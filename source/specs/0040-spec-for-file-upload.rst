File upload spec
================

Option 1: Use plain swift
-------------------------

In this option we'll just use: https://github.com/blacktorn/django-storage-swift.

Pros
####

* Least work
* No code on our end  (this doesn't mean no work).

Cons
####

* What if user uploads 3GB file, and interrupts this upload 10 times? We'd have 20GB of
  stale storage.
* Extra deployment complexity to handle big file uploads ... Handling uploads around 4GB
  is not very easy.

Complexity to handle big uploads
################################

Let's say end-user uploads 10GB file to our system:

1. Upload is first handled by nginx, which stores it to temporary file.
2. When upload is done all request is forwarded to uwsgi, there django will store this file in
   a temp file once again.
3. After the file is received it is sent from Django to external service.

To sum it up: file is copied three times creating two temp-files.

.. note::

   By default nginx doesn't handle uploads that big, some settings tweaking will be needed.

Manuals to upload large files are here:

* For owncloud, but nginx config should be the same:
  https://github.com/owncloud/documentation/wiki/Uploading-files-up-to-16GB
* Outdated, but mentioned in a couple of places:
  https://coderwall.com/p/swgfvw/nginx-direct-file-upload-without-passing-them-through-backend


Option 2: Use swift with direct uploads
---------------------------------------

In this solution files are uploaded directly from client to SWIFT.

Pros
####

* Fastest
* Theoretically no file size limit (practically 5GB, see Cons)

Cons
####

* Swift has hard limit on single upload (5GB) this is not limit on file size, but a limit on single
  chunk size, it an be worked around by uploading files in chunks, which would be a pain to do

  See this: http://docs.openstack.org/developer/swift/overview_large_objects.html

Implementation notes
####################


Configure CORS middleware
^^^^^^^^^^^^^^^^^^^^^^^^^

CORS is a tool that allows JavaScript code to do requests to other domains. So for our JavaScript
to be able to access (or edit) resources swift must send proper CORS headers.

This is controlled by CORS middleware, which is enabled on OctaWave.

To configure CORS you'll need to issue following command:

.. code-block:: bash

    swift post <container-name> \
               -H 'X-Container-Meta-Access-Control-Allow-Origin:*' \
               -H 'X-Container-Meta-Access-Control-Expose-Headers:*' \
               -H 'X-Container-Meta-Access-Control-Allow-Headers: *' \
               -A .... -U ... -K ...


To check if CORS is configured on container upload something to that container and
use this page for tests

.. code-block:: html

    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>Test CORS</title>
      </head>
      <body>

        Token<br><input id="token" type="text" size="64"><br><br>

        Method<br>
        <select id="method">
            <option value="GET">GET</option>
            <option value="HEAD">HEAD</option>
            <option value="POST">POST</option>
            <option value="DELETE">DELETE</option>
            <option value="PUT">PUT</option>
        </select><br><br>

        URL (Container or Object)<br><input id="url" size="64" type="text"><br><br>

        <input id="submit" type="button" value="Submit" onclick="submit(); return false;">

        <pre id="response_headers"></pre>
        <p>
        <hr>
        <pre id="response_body"></pre>

        <script type="text/javascript">
          function submit() {
              var token = document.getElementById('token').value;
              var method = document.getElementById('method').value;
              var url = document.getElementById('url').value;

              document.getElementById('response_headers').textContent = null;
              document.getElementById('response_body').textContent = null;

              var request = new XMLHttpRequest();

              request.onreadystatechange = function (oEvent) {
                  if (request.readyState == 4) {
                      responseHeaders = 'Status: ' + request.status;
                      responseHeaders = responseHeaders + '\nStatus Text: ' + request.statusText;
                      responseHeaders = responseHeaders + '\n\n' + request.getAllResponseHeaders();
                      document.getElementById('response_headers').textContent = responseHeaders;
                      document.getElementById('response_body').textContent = request.responseText;
                  }
              }

              request.open(method, url);
              if (token != '') {
                  // custom headers always trigger a pre-flight request
                  request.setRequestHeader('X-Auth-Token', token);
              }
              request.send(null);
          }
        </script>

      </body>
    </html>


Use Swift Form
^^^^^^^^^^^^^^

0. Set a crypthgraphic key for your infrastructure (see "Temp Url" section)
1. Prepare form with cryptographic signature:
   http://docs.openstack.org/developer/swift/api/form_post_middleware.html
2. Signature can be generated by this script. Key is set account-wide(??)

   .. code-block:: python

        import hmac
        from hashlib import sha1
        from time import time
        path = '/v1/AUTH_385fff76-290b-43da-b2fc-96b1c08bce24/xxxxx/foo'
        redirect = 'https://ocs-pl.oktawave.com'
        max_file_size = int(4.5 * 1024 * 1024 * 1024)
        max_file_count = 10
        expires = int(time() + 600)
        key = '...'
        hmac_body = '%s\n%s\n%s\n%s\n%s' % (path, redirect,
        max_file_size, max_file_count, expires)
        signature = hmac.new(key, hmac_body, sha1).hexdigest()
        print(max_file_size, max_file_count, expires, signature)

3. Enter information printed by this form to following HTML form:

   .. code-block:: html

        <html>
        <body>
            <form action="..."
                  method="POST"
                  enctype="multipart/form-data">
              <input type="hidden" name="redirect" value="https://ocs-pl.oktawave.com"/>
              <input type="hidden" name="max_file_size" value="4831838208"/>
              <input type="hidden" name="max_file_count" value="10"/>
              <input type="hidden" name="expires" value="1476875941"/>
              <input type="hidden" name="signature" value="..."/>
              <input type="file" name="FILE_NAME"/>
              <br/>
              <input type="submit"/>
            </form>
        </body>
        </html>

3. This has been tested. **Probably** this should work also for Javascript.

Use Temp Urls
^^^^^^^^^^^^^

1. Cryptographic keys are stored on either account or container level,
   To set one execute following command:

   .. code-block:: bash

        swift post <container-name> -H "X-Container-Meta-Temp-URL-Key:MYKEY"


2. Generate temp url for each upload, this can e done from fallowing code:

   .. code-block:: python

        import hmac
        from hashlib import sha1
        from time import time
        method = 'GET'
        duration_in_seconds = 60*60*24
        expires = int(time() + duration_in_seconds)
        path = '/v1/AUTH_385fff76-290b-43da-b2fc-96b1c08bce24/xxxxx/debian.iso'
        key = 'b3968d0207b54ece87cccc06515a89d4'
        hmac_body = '%s\n%s\n%s' % (method, expires, path)
        sig = hmac.new(key, hmac_body, sha1).hexdigest()
        s = 'https://{host}{path}?temp_url_sig={sig}&temp_url_expires={expires}'
        url = s.format(host='ocs-pl.oktawave.com', path=path, sig=sig, expires=expires)
        print url

2. Issue PUT from javascript (tested with ``curl`` and works)


Check cors and tempurl from curl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Set up cors on container
2. Create tempurl for PUT for new (notexistent file)
3. Issue following command:

   .. code-block:: bash

         curl -i -X OPTIONS -H 'Origin: http://localhost' \
         -H "Access-Control-Request-Method: PUT"
         temp url

4. You should get following response:

   .. code-block:: bash

        HTTP/1.1 200 OK
        access-control-allow-origin: *
        access-control-allow-methods: HEAD, GET, PUT, POST, COPY, OPTIONS, DELETE



Option 3: Direct upload to Swift with chunks
--------------------------------------------

The same as 2 but have additional support for large chunks:
http://docs.openstack.org/developer/swift/overview_large_objects.html.

This would require some javascript work, but 5GB should be enough.

Option 4: Do some nifty hash magic
----------------------------------

Do some nifty things with hashes --- this would require downloading big files to our servers.

Notes
#####

* Someone created a simple django app for that, it's old and outdated;
  https://github.com/mmcardle/django_swift_direct


Common tasks
^^^^^^^^^^^^

Tasks that should be dane ir-regardles what solution we'll use:

1. Add mount /tmp on tmpfs on containers: https://docs.docker.com/compose/compose-file/#/tmpfs
2. Do some tool that deletes unreferenced files


Selected solution
-----------------

We will go with Option number 2, that is direct uploads from JavaScript, with hard 5 GB limit
(that is one of Swift constraint)

Proposed API
^^^^^^^^^^^^

1. Normal DjangoStorage API's should work, that is we should configure django-storages-swift.
2. There should be an user-visible API endpoint (both in Python and in WWW API) that would
   take: a ``Resource`` id, and a file name. It will return a temp url which user can
   directly upload to.

   This endpoint should ensure that:

    1. User will not override other user's file.
    2. File will be downloaded with the same name user has specified, for example upload files
       to following url: ``fun-with-maths/c00ffe/instruction.guide.pdf`` --- in this example
       ``instruction.guide.pdf`` is the file name, ``fun-with-maths`` is slugified
       ``Resource`` title, and ``c0ffe`` is a long random hexadecimal url (note: we should
       check if this directory does not exists).

3. Access rights: Right now only users that are authors of the resource will have rights to
   call this endpoint for a given resource.
