Specification and architecture for OLCMS
========================================

.. note::

    When something is labelled as "Not in MVP" it means that it won't be avilable in Minimum Viable
    Product --- i.e. not available in first version of the software.

.. contents:: Table of Contents

Open problems
-------------

Metadata translation
    Metadata should be translated, there are multiple ways to do it:

    * use some canned solution like: `django-modeltranslate <http://django-modeltranslation.readthedocs.io/en/latest/registration.html>`__
    * create some tree like structure where for each `Content Item` many rows with metadata in
      different languages will be provided.

    This is addressed by creating a discovery task to specify this part of the system.

What licensing model to use?
    Only Creative Commons licences or allow custom licenses?

    For now we'll use CC, then we'll ask the partners about their
    opinion.

What domains we will use?
    Domains are specific identifiers e.g. age group, discipline that categorize `Content Items`.

    For now we'll use age groups (resembling Polish school) and discipline.

Data model
----------

Introduction
************

Content in OLCMS repository is organized into separate pieces called ``Content Items`` which
represent logical resources available for teachers for course construction. ``Content Items``
are reusable across courses. ``Content Item`` does not store data itself, it acts as a container for
metadata and series of ``Resources``.

.. _content-item:


Content item
************

Content item is a logical resource usable for teachers. It
contains mainly metadata, actual data is stored in ``Resource``.

Content items contains:

* **Name**: name of the resource
* **Description**: description of the resource
* **Authors**: reference to ``Author``
* **Owner**: person who can change the resource, reference to users, or their organisation.
* **Type**: reference to material ``Type`` - material type  would be e.g. video, document.
* **Domain**: this are domain specific identifiers e.g. age group, discipline.
* **Tags**: miscellaneous tags, provided by users
* **Resources**: ``Resource``'s attached to this entity.
* **Image**: cover image for the resource
* **License**: license on witch metadata is published. It also serves as default license for resources - ``Resource`` has its own license field which is optional, if not specified ``Resource`` inherits ``Content Item`` license


Resource
********

Resource stores data. It can be linked to a ``Content Item``.

Resource Contains:

* **Type**: type of the resource, available resource types are dependent on
  the type of ``Content Item``.
* **Resource Role**: role of the ``Resource``, e.g. it is a manual, pedagogical help on how to use
  the material, supplementary material.
* **Type Specifier**: additionally specifies type of the resource (this feature will be added at later project stage).
* **Language**: language of this resource.
* **Description**: optional textual description.
* **Content**: resource content, data.
* **License**: optional, by default ``Resource`` inherits ``Content Item`` license

Note the difference between ``Type`` fields in ``Content Item`` and ``Resource`` - type of ``Resource`` specifies
physical type of data, e.g. link, video file, text file etc. Type of ``Content Item`` specifies logical resource type,
e.g. document, video, etc.

For example ``Video`` content item could provide link to video on youtube, downloadable video file and
subtitles in few languages. It would contain resources with following types:

* **Video Link**: link to youtube page of the video.
* **Video File**: video file for download of online viewing.
* **Audio Track**: audio track for video file (one resource of this type for each supported language).
* **Subtitle File**: text file with subtitles for video file (one resource of this type for each supported language).

There is also ``Resource Role`` field available in ``Resource`` which is similar to the ``Type`` field,
but it specifies role or use-case of the resource, for example pedagogical help, manual, test material, etc.


Domains
*******

List and types of domains will be determined after consultation with partners.

For now we'll have following domains:

* **Subject**
* **Age group** (roughly using polish school system): 6-10; 10-14; 14-18; 18+

License
*******

For now we'll start with Creative Commons (CC) license as default for content.

Resource role
*************

Resource role allows users to define what the resource is in a logical manner.

Example resource role:

* pedagogical guide
* test material
* accompanying material.

Another entities
****************

**Tags**
    Generic tags, provided by users.

**Owner**
    A reference to user or his organisation.

**Content Item Type**
    Type of content is set during ``Content Item`` creation, and defines most functionaries and
    behaviours of ``Content Item``, as well as available ``Resource Types``.

**Resource Type**
    Type of resource.


Basic principles
----------------

Content Item supports following operations:

* ``Create``: anyone can create content item; what information is needed to be set depends entirely on
  ``Content Type``.
* ``Edit``: only person that created content item can edit it (some other security scheme may be thought of)
* ``Publish``: each resource will have a draft version and published version. Draft will replace
  published version when user performs a ``publish`` action.
* ``View``: when resource is viewed it's rendering depends on ``Content Item Type``.
* ``Fork``: (not in MVP) sometimes an user needs to create resource similar to given resource,
  but with some changes --- to facilitate this he can ``Fork`` the resource --- creating a copy
  of given resource that he might edit.

List of features
----------------

Basic ``Resource Types``
************************

For every ``Content Item`` user will be able to attach following resources:

* ``File`` resources of various roles (for example pedagogical guide)

  Each file has a description and a content type.
  Content type (e.g. msword document, or excel spreadsheet)
  should be guessed from file content (when uploading), or can be set
  explicitly --- and is used to display information on how to use this file (e.g. what programs need
  to be installed).

* ``HTML`` resources (essentially the same as file resources, but created using a builtin WYSWIG editor).
* ``Link`` resource.

Simple Content Item
*******************

A ``Content Item`` that can contain:

* ``File`` resources
* ``HTML`` resources
* ``Link`` resources

It is rendered as a grid of possible downloads, with one item that is "Highlighted" or "Main".

.. note::

    It might be rendered differently in the view produced by Sequence Content Item, e.g.
    when it is rendered as a part of Sequence Content Item it only displays "Main" resource (and optionally
    a small list of links to rest of the resources at the bottom).

.. _video-content-full:

Video Content Item
******************

A ``Content Item`` that is rendered as a single video. It should have set following resources:

* ``Video Link`` resource: it contains a link to youtube --- we will by default embed this video when rendering.
* ``Video File`` resource: it contains a file that can be downloaded.
* ``Video Subtitles``
* ``Video Audio`` tracks

After user uploads the video to our servers we will upload the video to youtube to our channel
(this is the only way we can guarantee that uploaded version and youtube one are in sync).

LTI Content item
****************

A ``Content Item`` that accesses external resource using LTI (or similar protocol).
Essentially this is a remote service that allows user to perform some task
and then grades him.

Quiz Content Item
*****************

A ``Content Item`` that contains:

* Single choice questions
* Multi choice questions
* Open questions.

When user views this ``Content Item`` he might fill this questionnaire and he'll get a score.
This score is saved in the profile if he is logged in.

Quiz can be downloaded in pdf/docx/odt format for pen and paper tests.

Quizzes can be downloaded and imported to Moodle (and other CMS's)

.. note::

    Exact format of Quizzes is to be discussed. Most possibly we will use some off-the shelf software (if
    there is one sufficiently good)

.. _simulation-ci:

Simulation Content Item
***********************

``Simulation Content Item`` contains a ``Simulation Resource``, that will enable us to display
an interactive in-browser simulation.

.. note::

    Format must be specified, e.g. a ``.zip`` file containing a html file and resources.


.. _sequence-content-item:

Sequence Content Item
*********************

``Sequence Content Item`` contains a list of links to other ``Content Items``, that are displayed in
sequence. It could serve the role of very simple course.

For example to create a movie that lasts 30 minutes and has quiz in the middle and at the end of the film one
would create sequence containing :

* First part of the movie (``Video Content Item``)
* Quiz (``Quiz Content Item``)
* Second part of the movie (``Video Content Item``)
* Second Quiz (``Quiz Content Item``)

.. note::

    This is similar to what SCORM does (linear sequencing).

    Content items might be rendered differently by sequence content item.


Resource management features
----------------------------

Content Item versioning
***********************

Content items should be versioned: each change should transparently
create new version of the ``Content Item``. One should be able to revert to previous version.

Content Item forking
********************

When user (named Alice) wants to change resource created by another user (Bob)
she should be able to "Fork it". Forked resource a copy of the original resource
but Alice has rights to edit the copy. Forked resources are linked to original
resources (that is: Carol who visits the OLCMS and sees Alice's copy can
also see the link to Bob's copy).

Note when Alice forks content from Bob, author's list should be unchanged. It should be altered
to include Alice when Alice edits the content.

.. note::

    There are many issues with Forking, it interacts in surprising ways with rest of the system:

    1. How does Quiz answers behave when forking?
    2. How does Ratings behave when forking?

Content Item merging
********************

Alice forked Bobs Content Item, she can ask Bob to merge changes --- that is to update Bob's copy
with Alice's version.


Content lifecycle management
****************************

Add explicit publish step to content creation/edit.

Changes to content would be visible only after user clicks publish.

.. note::

    This would fit very well with content versioning. Maybe it would be worth to implement
    these two at the same time.

Translations
************

Translations are an open problem right now.

Content rating
**************

Ability to add rating to content, and comment it.

It must be figured out how rating and comments should interact with content forking.

Cross cutting features
----------------------

.. _search:

Search
******

Objects that are searched are ``Content Items``, that is: content items are results of the search.

User should be able to refine his search using:

* Type of content item (e.g. search only for simulations)
* Domain of content item (e.g. search for Physics content for high school)
* Language
* Contents of metadata (both for ``Content Item`` and resources)
* Full text search for resource contents.

.. _user-management:

User management
***************

Users should be able to login by ``username`` and ``password``, also using some selected Oauth2
providers (Google+, Facebook).

Login should be optional for read-only access to the page.

After login users should be able to provide profile where they signify whether they are a:

* teacher
* student
* adminstrator (Consortium Member role).

Students will not have (by default) most of the edit functionality of the page.
They may be (not in MVP) added to classes where they might be graded.


Proposed version list
---------------------


Preparatory tasks
*****************

* Investigate content formats and select content formats we'd need to export to
* Write technical requirements
* Figure out language versions
* Do proper architecture discovery
* Project set up



Bare bones system
*****************


    **Timeline**: At 31st of January 2017 we should
    release first version of OLCMS that implements all of these features, as well
    some features from: "Minimal version".

* User can see list of content items and search them by metadata.
  Search is :ref:`described here <search>`.
  Content item is described here :ref:`Content item <content-item>`.
* User is able to view each individual content item.
* User is able to log in (:ref:`user management <user-management>`).
* Logged in user is able to upload a :ref:`Basic Content Item <basic_content_item>`
  with :ref:`basic resource <basic-resource>`. ``Basic Content Item``
  contains: files, html pages and links.
* UI is bilingual polish/english.
* Content Items are in a single language.

Minimal version
***************

.. note::

    This should be finished by 31st of May 2017.

* User can upload, edit and see :ref:`Video Content Item in minimalistic version <video-v1>`.
  (just video, no subtitles; subtitles will be added in next version)
* User can upload, edit and see Quiz Content item in minimalistic version
  (ability to define quiz, and take quiz interactively). See :ref:`Quiz v1 <quiz-v1>`
* Multilingual content items
* Ability to rate and comment content items.
* User can create :ref:`sequences of content items<sequence-content-item>`.
* User is able to perform a full-text search on content (our ultimate goal is to
  have everything searchable --- including video subtitles, simulation text and so forth.
  At this point in time though not all content types are implemented: so obviously they
  will not be searchable. However Full-Text will be implemented for these content types
  as they are developed)

Baseline version
****************

.. note::

    Target date September 2017

* Full blown :ref:`Video Content item <video-content-full>`;
  user can add :ref:`subtitles <video-sub>`.
* :ref:`Quiz v2 <quiz_phase2>`; user can download quiz in various formats
* Create a :ref:`simulation content item <simulation-ci>`

Un-prioritized features
***********************

These features are not prioritized and have no task assigned to them

* Add student/teacher roles to the OLCMS and display different content kinds for both of them.
* Decide whether add typical LMS features (classes, grading); or integrate with a off-the-shelf LMS.
* Quiz v3 (integration with full-blown LMS)
* Create multilevel content --- containing section, subsection and paragraph.
* Export content items to LMS (like Moodle). Export multilevel content, export Quizzes.
* Content forking.
* Content merging
* Content versioning
* Add lifecycle management to content items
* LTI content item

.. _feature-details:

Stories definition
------------------

In this section work is slitted to manageable stories --- each of them is doable
by a single developer in manageable amount of time.

Preparatory tasks
*****************

Investigate content formats and select content formats we'd need to export to
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Read about standardized content formats used by many LMSes and decide
which one should we use.

Outline any possible changes to data model we'd need make to facilitate
this format.

We needs formats for two things:

* Quizzes
* Sequence Content Item (Sequences can contain quizzes)

Write technical requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Write out any sensible technical requirements, for me it's:

1. OLCMS has a REST API, that allows everything GUI allows.
2. OLCMS GUI uses this api.
3. Close to 100% test coverage
4. Linter and pep8 enforced

Do proper architecture discovery
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Install Jackrabbit repository and check if it is better than other solutions (ie. storing
  things directly in Swift)
* Figure out language versions / Do some prototyping with translations
* Decide final database layout
* Do some prototyping/reading about full-text search
  (
    please check at least:
    `postgres full text search <https://www.postgresql.org/docs/9.5/static/textsearch.html>`__,
    `django postgres full text <https://docs.djangoproject.com/en/1.10/ref/contrib/postgres/search/>`__,
    `elasticsearch/lucene <https://en.wikipedia.org/wiki/Elasticsearch>`__
  ).

  Please seriously consider using postgres --- it's one less service to maintain!
* Select technology (Django)
* Decide if we want to integrate with some off-the-shelf lms
* Select python version and Django version
  (off-the-shelf lms may only support specific Django/Python versions).
* Decide whether we do Single Page APP or no.



Figure out language versions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Figure out how to handle translations for Content Item and Resource.

Metadata should be translated, there are multiple ways to do it:

    * Use some canned solution like: `django-modeltranslate <http://django-modeltranslation.readthedocs.io/en/latest/registration.html>`__
    * Create some tree like structure where for each ``Content Item`` many rows with metadata in
      different languages will be provided.

Project set up
^^^^^^^^^^^^^^

* Create a repository
* Set up project skeleton
* Set up CI
* Set up linter
* Create some base template

Bare bones version
******************

Create models for login and partners
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Set up login and user registration
* Create model for partner organisation.
* Create GUI for adding partners

Create utility to store files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Think of nice way to store files, requirements would be:

1. Files are immutable --- after they are created they should not change.
2. Copies to files are shallow --- that is if someone uploads a 1GB movie to our
   servers and someone else copies the resource we'd rather not have two copies
   of this file.

Maybe store files using sha256 reference --- however computing them for large files
could be not very easy.

Create following features:

1. Create models to store files.
2. Create API to store files.


.. _basic-resource:

Create Basic resources
^^^^^^^^^^^^^^^^^^^^^^

Create views that allow:

* Adding basic resource
* Viewing basic resource

For following resources:

* File Resource
* HTML Page Resource
* Link Resource

.. note::

    If this turns out too big just split off part of work.


.. _basic_content_item:

Create Basic Content Item
^^^^^^^^^^^^^^^^^^^^^^^^^

Create Basic Content item --- a collection of basic resources.

Make as much of the work re-usable for Video Content Item and so forth.

At the end of this task user should be able to create,
view and edit the content item.

This is too big for a single story, proposed split:

1. Create models to facilitate Content Item and form to add and edit it.
2. Create a view that renders the content item

Create a Content Item List
^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a page that lists all Content Items, this page is paginated.

Add basic search to that page.

Prepare platform translations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Inform the partners that we will be doing the platform in Polish and English, ask them if they
   would like to participate in translation efforts.
2. If they do select proper translation tool, like: https://www.transifex.com (expensive),
   https://www.oneskyapp.com/, http://zanata.org/
3. Prepare polish translations in the tool and encourage partners to contribute.

Decide what licensing model to use
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Draft a proposal and ask partners for input

Decide what domains should we define
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Draft a proposal and ask partners for input


Set up youtube account
^^^^^^^^^^^^^^^^^^^^^^

Create Youtube account with API access for the project.

Coordinate with partners about branding/logo etc.

Initial version
***************

.. _video-v1:

Create Video Content Item v1
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a video resource.

1. User should need to upload the video (maybe(?) video upload should need
   special privilege).
2. Our system would upload the video to youtube.

.. _quiz-v1:

Create a Quiz Content Item v1
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a Quiz Content item. Acceptance Criteria:

1. User may add Quizzes that contain Multi Choice Questions, and Single Choice Questions,
   and (ungraded) Text Fields.
2. Users who take the quiz get grade a percentage grade displayed.
3. Responses and grades are recorded somewhere.

This is too big for a single story, proposed split:

1. Create models for the resource resource and UI to add questions.
2. Create a view that displays the quiz
3. Create UI to take the quiz.

Add full-text search to Content Items
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Full text search should extract contents from files.

Create spec for simulation content item
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a spec for simulation content item
and then give it to partners for approval.

Implement simulation Content Item
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Implement simulation Content Item

Baseline version
****************


.. _video-sub:

Add ability to upload subtitles to Videos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* There should be some validation done (e.g. check if subtitle track is not longer than the video;
  format checks for subtitle file).
* Subtitles should be synced to youtube.

.. _quiz_phase2:

Quiz v2
^^^^^^^

1. Allow the owner of Content Item to see quiz responses.
2. Allow quiz download to odt/docx/pdf whatnot.

Add student/teacher roles
^^^^^^^^^^^^^^^^^^^^^^^^^

Add student/teacher roles to the OLCMS and display different content kinds for both of them.

Create a simulation content item
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This contains multiple tasks:

1. Create a spec for simulation content item
2. Have it approved by partners
3. Implement it.

Full text search
^^^^^^^^^^^^^^^^

This contains multiple tasks:

1. Check out results of the initial discovery about full text search.
2. Create full text search indexes.
3. Add search support.
