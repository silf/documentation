Translating (i18n) Remote lab Discovery
=======================================

Concerns for all solutions
--------------------------

1. Generally user should specify more than one language (s)he speaks, maybe
   accept the same syntax as `Accept-Language header
   <https://pl.wikipedia.org/w/index.php?title=Lista_nag%C5%82%C3%B3wk%C3%B3w_HTTP&oldid=46310106#Accept-Language>`_?


Translation (i18n) for Remote Lab
---------------------------------

Possible solutions
******************

**1. Make Experiment do all the work**
    In this solution operator will send to the server what language(s) (s)he. Server then
    tries to accommodate to the language choice.

    **Pros**

    * Least effort to implement
    * Very little changes to architecture

    **Cons**

    * In an experiment session language is chosen by the operator, and rest of the users
      end with a unintelligible interface. OTOH accommodating multi-language experiment sessions
      would be hard.

**2. Make experiment publish translatable strings and translation resources**
    In this solution experiment publishes translatable strings, instead:

    .. code-block:: json

        "label": "Impulse count for 10 sec measurement"

    It publishes:

    .. code-block:: json

        "label": {
            msg: "Impulse count for {time} sec measurement"
            ctx: {
                time: 10
            }

    And publishes translation that translate "Impulse count for {time} sec measurement"
    to various languages.

    These translations can be delivered to clients in two ways:

    1. Either from ``silf-app``, where they are uploaded by operators when Experiments
       are changed.
    2. From the experiments themselves.

**3. Make experiment publish every language**
    In this solution Experiment publishes translation strings for all languages it supports,
    for example instead of

    .. code-block:: json

        "label": "Czas pomiaru dla jednej grubo\u015bci materia\u0142u"

    It sends:

    .. code-block:: json

        "label": {
            "pl": "Czas pomiaru dla jednej grubo\u015bci materia\u0142u",
            "en": ...
            "de": ...
        }

    **Cons**

    * Not really a good solution.
    * Complicates SILF in unexpected ways --- for example when storing experiment results.

Verdict
*******

Option 1. is by far easiest. We will go with it.

Proposed tasks
--------------

Prepare draft of protocol changes required
******************************************

* Prepare draft of protocol changes
* Have it approved

Prepare a an extraction tool that extracts strings from Experiment
******************************************************************

There is a lot of OSS tools that do this, choose one of them and:

* Document how it should be used.
* (Maybe) prepare some wrapper scripts (push them to commons repository).
* Extract strings from one of the experiments, and prepare them
  for translation (translate this to english along the way).

Prepare translations (for each of experiments)
**********************************************

The same as above but for the each new experiment.

Add support for this feature to commons
***************************************

Implement protocol in commons and add it to one of the experiments.

Add support for this feature in Mercury
***************************************

Implement protocol in Mercury.

Translation for Chatbot
-----------------------

Is totally not obvious. Will be done at a later date.
