How to implement quiz resource
==============================


Selected solution
-----------------

I think that using GIFT or QTI is not an option. So I propose using NUMBAS
(all will be explained shortly), with following assumptions:

1. Integrate numbas player into OLCMS --- at this time we will ask instructors
   to prepare quizzes in numbas public instance: https://numbas.mathcentre.ac.uk/
2. Think about integration with the editor.


Tasks for phase 1
-----------------

1. Create resource type description ``Simulation Content Item`` (this is
   an attempt to kill two birds with one stone). This content item contains a
   zip file containing ``index.html`` and some assets.

   When you see this resource you are taken a page where ``index.html``
   is rendered. This might be rendered inside iframe(?).
2. Create quiz resource that contains zip file downloaded from NUMBAS editor.

   Zip file also contains exam json source.


Tasks for phase 2
-----------------

1. Create editor docker image.
2. Deploy numbas editor on our infrastructure.
3. Add our templates to numbas infrastructure.
4. Update numbas to store static files in OCS.

   1. Another solution would be to allow numbas to export files with all dependencies.

4. Update numbas generator to use static files in OCS.

Numbas description
------------------

Numbas consists of a lot of repositories:

1. Numbas editor: a **django** application that allows quiz edition.
   This quiz editor creates "exams" in following formats:

   1. Json file (that **does not contain everything needed to compile this to output format**).
   2. SCORM package
   3. Zip package with index html (see :download:`2.X <quiz-data/exam-31648-introduction-to-stresses.zip>`.

2. Numbas compiler: a pure python script that renders json files above to both formats below.

   To convert ``exam`` json to other formats you need:

   * json file
   * All resources that are referenced inside json file (images, etc).
   * All javascript plugins referenced inside json file.

   It looks like editor and compiler are assumed to be on the same machine.
   (we may need to fix that).

   Conversion is done by some jinja templates that are easily customisable.

3. A lot of plugins.

Possible solutions
------------------

Use third party quiz format (GIFT, QTI)
***************************************

This is most probably not an option as finding off-the shelf editors and or players for these
formats is not really possible.

See below for specific problems with GIFT and QTI.

**PROS**

* We'll be standard compliant

**CONS**

* I don't believe it will work.

Use homebrewn solution
**********************


Create own format for quizzes (separate from OLCMS) that consists of following elements

a. JSON data format for quizz data
b. JS components for editing and rendering contents
c. API specification that allows answer submission and grading display

Requirements (need to be specified more):

1. Allow single choice, multi choice questions, text input and long text input (not graded) Allow. "Text Questions"
2. Student should not get the JSON with answers.
3. Some rudimentary grading
4. Simple randomization (randomize question order, randomize answer order, select N of questions in quiz).

**CONS**

* Much work

**PROS**

* Possible to estimate.
* We will be lacking features!
* Won't be standard compliant (but I think we'll be able to write some transformation code)


Use third party quiz solution
*****************************

Turns out that there are some nice quiz solutions:

* https://github.com/numbas/Numbas
* https://github.com/QuickenLoans/SlickQuiz


**PROS**

* Less work for features gained

**CONS**

* Possible to estimate, but not that easy to estimate. Won't integrate in that well in OLCMS.
* Won't be standard compliant

NUMBAS evaluation
-----------------

Mainly javascript (without any compoment framework), with python backend.

Overally this is a **very nice** project,

* Sensible breakdown in API and engine (two repositories!).
* Backend in python and django
* Frontend in pure jQuery,
* Exports "SCORM compliant" packages.
* JSON Exam format.

SlickQuiz Evaluation
--------------------

A nice javascript library that display quizzes. Has it's own JSON question format.

**PROS**

* We'd have to write only editor for this format.

**CONS**

* Javascript is used to validate the questions so this won't be tamper-proof.

GIFT format Evaluation
----------------------

Evaluation of format itself.

This format is positively awfull, I don't want to touch it with a yard long
stick. Example question looks like this:

.. code-block::

    What's the answer to this multiple-choice question? {
      ~wrong answer#feedback comment on the wrong answer
      ~another wrong answer#feedback comment on this wrong answer
      =right answer#Very good!
    }

    //From The Hitchhiker's Guide to the Galaxy
    Deep Thought said " {
      =forty two#Correct according to The Hitchhiker's Guide to the Galaxy!
      =42#Correct, as told to Loonquawl and Phouchg
      =forty-two#Correct!
    }  is the Ultimate Answer to the Ultimate Question of Life, The Universe, and Everything."

       42 is the Absolute Answer to everything.{
    FALSE#42is the Ultimate Answer.#You gave the right answer.}

If we want to produce GIFT from our code, it would be easier to use proprietary
format (eg. json variant) and then convert to GIFT.


Gift editors
************

``https://github.com/PedroBlanco/gift-editor``
    A pure js gift editor.

    Pros:

    * Seems to work
    * In pureish js.

    Cons:

    * Everything is in Spanish/Portuguese (UI, documentation)
    * Almost no documentation
    * Last commit two years ago.
    * In jQuery not in any framework.

``https://github.com/mbolis/moodle-quiz-builder``
  Pure js gift editor

  Pros:

  * Uses ``vue.js``
  * In english

  Cons:

  * Seems not complete



QTI Format evaluation
---------------------

There are couple of versions of the standard I focused on ``2.2`` (newest one)
and (``1.2 lite``) as it is supposed to be easy.

Both are very fine examples of design by comitee and are I would not attempt to
re-implement them .

Example question XML can be downloaded for both versions: :download:`1.2 lite <quiz-data/qti-1-2.xml>`,
:download:`2.X <quiz-data/qti-2-X.xml>`.

We don't have the resources to implement either editor or player for any of these formats,
and moreover --- ``1.2 lite`` is **very limited** it supports:
The only question-types to be supported within QTILite are:

* Yes/No
* True/false
* Likert scale examples could be: strongly agree, agree, neutral, disagree, strongly disagree, strongly agree, agree, disagree, strongly disagree, agree, neutral and disagree
* Other forms of multiple choice (i.e. one choice from many);

There are **many tools** that work with QTI,


QTI Tools that we can consider
******************************

``TAO``
    A large maintaned package https://www.taotesting.com/tao-community-edition/

    I'm not sure if integrating such a big project right now would be a good solution.



QTI tools that are not good enough
**********************************

``qti-player``
    Project looks nice, is in Javascript. But is dead and demo doesn't work: https://github.com/etavener/qti-player

``qti-magic``
    QTIMagic is an open source assessment authoring tool. The tool supports the draft specification QTI 2.1 PD2. Quizzes, exams, surveys, etc. can be authored using this tool, uploaded to a compatible assessment engine, and executed by that engine.

    In Java + JSP + Servlets. Not maintained.




